<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use MongoDB\BSON\Regex as MongoRegex;
use Log;

class MtgController extends Controller
{
    /**
     *
     */
    public function search (Request $request) {

        require '/var/www/html/Laravel0922/vendor/autoload.php';
        $client = new \MongoDB\Client("mongodb://localhost:27017");
        $collection = $client->mtgjson->ZNR;

        /**
         * リクエストデータの取得
         */
        $request_data = [];
        $request_data['keywords'] = null;
        if (!empty($request->input('keywords'))) {
            $request_data['keywords'] = $request->input('keywords');
        }

        $request_data['page'] = null;
        if (!empty($request->input('page'))) {
            $request_data['page'] = $request->input('page');
        }

        /**
         * ページ設定
         */
        $page = (isset($request_data['page'])) ? $request_data['page'] : 1 ;
        $page_per = 10;
        $skip = ($page - 1) * $page_per;

        /**
         *
         */
        $keyword_query = [];
        if ($request_data['keywords']) {
            $keyword_query = ['name' => new MongoRegex($request_data['keywords'], 'i')];
        }

        /**
         * データの取得
         */
        $data = $collection->find($keyword_query, [
            'limit' => $page_per,
            'skip' => $skip,
        ])->toArray();

        /**
         * 総件数の取得
         */
        $count = $collection->count(
            $keyword_query
            , [

        ]);

        $page_total = (int)ceil($count/$page_per);

        $result = [
            'cards' => $data,
            'last_page' => $page_total,
        ];

        return $result;

    }
}
