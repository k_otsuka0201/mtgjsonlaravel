<?php
ini_set("display_errors", 1);
error_reporting(E_ALL);

require '../vendor/autoload.php'; // include Composer's autoloader
$client = new MongoDB\Client("mongodb://localhost:27017");
//var_dump($client);

$collection = $client->mydb->users;
//var_dump($collection);

$count = $collection->count();
//var_dump($count);

$data = $collection->find(['name' => 'tarou'])->toArray();

header('content-type: application/json; charset=utf-8');
echo json_encode($data);
